﻿namespace Terrarium_GUI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.timeGroup = new System.Windows.Forms.GroupBox();
            this.timeNowButton = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.timeSaveButton = new System.Windows.Forms.Button();
            this.timeEditButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.timeDateControl = new System.Windows.Forms.DateTimePicker();
            this.timeDayEndControl = new System.Windows.Forms.TextBox();
            this.timeDayStartControl = new System.Windows.Forms.TextBox();
            this.timeTimeControl = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.otherGroup = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.updateHistoryControl = new System.Windows.Forms.NumericUpDown();
            this.updateSensorsControl = new System.Windows.Forms.NumericUpDown();
            this.updateEditButton = new System.Windows.Forms.Button();
            this.updateSaveButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pumpSaveButton = new System.Windows.Forms.Button();
            this.pumpEditButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.pumpLastActiveControl = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.pumpActiveForControl = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.pumpActivatedWaitControl = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.addressControl = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.humiSaveButton = new System.Windows.Forms.Button();
            this.humiEditButton = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.humiMaxControl = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.humiMinControl = new System.Windows.Forms.NumericUpDown();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tempMaxControl = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tempSaveButton = new System.Windows.Forms.Button();
            this.tempEditButton = new System.Windows.Forms.Button();
            this.tempMinControl = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.tempValueLabel = new System.Windows.Forms.Label();
            this.humiValueLabel = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.temperaturePanel = new System.Windows.Forms.Panel();
            this.tempLoHiLabel = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.humiLoHiLabel = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.temperaturePanel2 = new System.Windows.Forms.Panel();
            this.temp2LoHiLabel = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.temp2ValueLabel = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.timeFromToLabel = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.timeValueLabel = new System.Windows.Forms.Label();
            this.tempGroup = new System.Windows.Forms.GroupBox();
            this.humiGroup = new System.Windows.Forms.GroupBox();
            this.temp2Group = new System.Windows.Forms.GroupBox();
            this.temp2EditButton = new System.Windows.Forms.Button();
            this.temp2SaveButton = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.temp2MinControl = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.temp2MaxControl = new System.Windows.Forms.NumericUpDown();
            this.label30 = new System.Windows.Forms.Label();
            this.timeGroup.SuspendLayout();
            this.otherGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateHistoryControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateSensorsControl)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pumpActiveForControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpActivatedWaitControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humiMaxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humiMinControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempMaxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempMinControl)).BeginInit();
            this.temperaturePanel.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.temperaturePanel2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tempGroup.SuspendLayout();
            this.humiGroup.SuspendLayout();
            this.temp2Group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temp2MinControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp2MaxControl)).BeginInit();
            this.SuspendLayout();
            // 
            // timeGroup
            // 
            this.timeGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timeGroup.Controls.Add(this.timeNowButton);
            this.timeGroup.Controls.Add(this.panel5);
            this.timeGroup.Controls.Add(this.timeSaveButton);
            this.timeGroup.Controls.Add(this.timeEditButton);
            this.timeGroup.Controls.Add(this.label8);
            this.timeGroup.Controls.Add(this.timeDateControl);
            this.timeGroup.Controls.Add(this.timeDayEndControl);
            this.timeGroup.Controls.Add(this.timeDayStartControl);
            this.timeGroup.Controls.Add(this.timeTimeControl);
            this.timeGroup.Controls.Add(this.label7);
            this.timeGroup.Controls.Add(this.label6);
            this.timeGroup.Location = new System.Drawing.Point(260, 4);
            this.timeGroup.Name = "timeGroup";
            this.timeGroup.Size = new System.Drawing.Size(542, 394);
            this.timeGroup.TabIndex = 2;
            this.timeGroup.TabStop = false;
            this.timeGroup.Visible = false;
            // 
            // timeNowButton
            // 
            this.timeNowButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeNowButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeNowButton.Location = new System.Drawing.Point(380, 340);
            this.timeNowButton.Name = "timeNowButton";
            this.timeNowButton.Size = new System.Drawing.Size(75, 22);
            this.timeNowButton.TabIndex = 41;
            this.timeNowButton.Text = "Now";
            this.timeNowButton.UseVisualStyleBackColor = true;
            this.timeNowButton.Visible = false;
            this.timeNowButton.Click += new System.EventHandler(this.timeNowButton_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Location = new System.Drawing.Point(6, 14);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(530, 320);
            this.panel5.TabIndex = 37;
            // 
            // timeSaveButton
            // 
            this.timeSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeSaveButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeSaveButton.Location = new System.Drawing.Point(380, 365);
            this.timeSaveButton.Name = "timeSaveButton";
            this.timeSaveButton.Size = new System.Drawing.Size(75, 23);
            this.timeSaveButton.TabIndex = 40;
            this.timeSaveButton.Text = "Save";
            this.timeSaveButton.UseVisualStyleBackColor = true;
            this.timeSaveButton.Visible = false;
            this.timeSaveButton.Click += new System.EventHandler(this.timeSaveButton_Click);
            // 
            // timeEditButton
            // 
            this.timeEditButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeEditButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeEditButton.Location = new System.Drawing.Point(461, 365);
            this.timeEditButton.Name = "timeEditButton";
            this.timeEditButton.Size = new System.Drawing.Size(75, 23);
            this.timeEditButton.TabIndex = 39;
            this.timeEditButton.Text = "Edit";
            this.timeEditButton.UseVisualStyleBackColor = true;
            this.timeEditButton.Click += new System.EventHandler(this.timeEditButton_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(97, 343);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Date";
            // 
            // timeDateControl
            // 
            this.timeDateControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeDateControl.Enabled = false;
            this.timeDateControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeDateControl.Location = new System.Drawing.Point(134, 340);
            this.timeDateControl.MaxDate = new System.DateTime(2038, 1, 1, 0, 0, 0, 0);
            this.timeDateControl.MinDate = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            this.timeDateControl.Name = "timeDateControl";
            this.timeDateControl.Size = new System.Drawing.Size(170, 22);
            this.timeDateControl.TabIndex = 19;
            // 
            // timeDayEndControl
            // 
            this.timeDayEndControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeDayEndControl.Enabled = false;
            this.timeDayEndControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeDayEndControl.Location = new System.Drawing.Point(310, 366);
            this.timeDayEndControl.Name = "timeDayEndControl";
            this.timeDayEndControl.Size = new System.Drawing.Size(54, 22);
            this.timeDayEndControl.TabIndex = 17;
            this.timeDayEndControl.Text = "21:00";
            this.timeDayEndControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timeDayStartControl
            // 
            this.timeDayStartControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeDayStartControl.Enabled = false;
            this.timeDayStartControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeDayStartControl.Location = new System.Drawing.Point(163, 366);
            this.timeDayStartControl.Name = "timeDayStartControl";
            this.timeDayStartControl.Size = new System.Drawing.Size(54, 22);
            this.timeDayStartControl.TabIndex = 16;
            this.timeDayStartControl.Text = "08:00";
            this.timeDayStartControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timeTimeControl
            // 
            this.timeTimeControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.timeTimeControl.Enabled = false;
            this.timeTimeControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeTimeControl.Location = new System.Drawing.Point(310, 340);
            this.timeTimeControl.Name = "timeTimeControl";
            this.timeTimeControl.Size = new System.Drawing.Size(54, 22);
            this.timeTimeControl.TabIndex = 15;
            this.timeTimeControl.Text = "13:00";
            this.timeTimeControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(247, 369);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Day\'s end";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(97, 369);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Day\'s start";
            // 
            // otherGroup
            // 
            this.otherGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.otherGroup.Controls.Add(this.groupBox2);
            this.otherGroup.Controls.Add(this.groupBox1);
            this.otherGroup.Controls.Add(this.label11);
            this.otherGroup.Controls.Add(this.addressControl);
            this.otherGroup.Location = new System.Drawing.Point(260, 4);
            this.otherGroup.Name = "otherGroup";
            this.otherGroup.Size = new System.Drawing.Size(542, 394);
            this.otherGroup.TabIndex = 3;
            this.otherGroup.TabStop = false;
            this.otherGroup.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.updateHistoryControl);
            this.groupBox2.Controls.Add(this.updateSensorsControl);
            this.groupBox2.Controls.Add(this.updateEditButton);
            this.groupBox2.Controls.Add(this.updateSaveButton);
            this.groupBox2.Location = new System.Drawing.Point(51, 189);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(444, 130);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Updating";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(25, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "Update interval of Sensors";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(24, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(139, 13);
            this.label17.TabIndex = 49;
            this.label17.Text = "Update interval of History";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(81, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 13);
            this.label5.TabIndex = 48;
            this.label5.Text = "s";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(80, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "s";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // updateHistoryControl
            // 
            this.updateHistoryControl.Enabled = false;
            this.updateHistoryControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateHistoryControl.Location = new System.Drawing.Point(27, 46);
            this.updateHistoryControl.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.updateHistoryControl.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.updateHistoryControl.Name = "updateHistoryControl";
            this.updateHistoryControl.Size = new System.Drawing.Size(47, 22);
            this.updateHistoryControl.TabIndex = 45;
            this.updateHistoryControl.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // updateSensorsControl
            // 
            this.updateSensorsControl.Enabled = false;
            this.updateSensorsControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateSensorsControl.Location = new System.Drawing.Point(28, 87);
            this.updateSensorsControl.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.updateSensorsControl.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.updateSensorsControl.Name = "updateSensorsControl";
            this.updateSensorsControl.Size = new System.Drawing.Size(47, 22);
            this.updateSensorsControl.TabIndex = 46;
            this.updateSensorsControl.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // updateEditButton
            // 
            this.updateEditButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateEditButton.Location = new System.Drawing.Point(363, 101);
            this.updateEditButton.Name = "updateEditButton";
            this.updateEditButton.Size = new System.Drawing.Size(75, 23);
            this.updateEditButton.TabIndex = 43;
            this.updateEditButton.Text = "Edit";
            this.updateEditButton.UseVisualStyleBackColor = true;
            this.updateEditButton.Click += new System.EventHandler(this.updateEditButton_Click);
            // 
            // updateSaveButton
            // 
            this.updateSaveButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateSaveButton.Location = new System.Drawing.Point(282, 101);
            this.updateSaveButton.Name = "updateSaveButton";
            this.updateSaveButton.Size = new System.Drawing.Size(75, 23);
            this.updateSaveButton.TabIndex = 44;
            this.updateSaveButton.Text = "Save";
            this.updateSaveButton.UseVisualStyleBackColor = true;
            this.updateSaveButton.Visible = false;
            this.updateSaveButton.Click += new System.EventHandler(this.updateSaveButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pumpSaveButton);
            this.groupBox1.Controls.Add(this.pumpEditButton);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.pumpLastActiveControl);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.pumpActiveForControl);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.pumpActivatedWaitControl);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Location = new System.Drawing.Point(51, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 162);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pump";
            // 
            // pumpSaveButton
            // 
            this.pumpSaveButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpSaveButton.Location = new System.Drawing.Point(282, 132);
            this.pumpSaveButton.Name = "pumpSaveButton";
            this.pumpSaveButton.Size = new System.Drawing.Size(75, 23);
            this.pumpSaveButton.TabIndex = 46;
            this.pumpSaveButton.Text = "Save";
            this.pumpSaveButton.UseVisualStyleBackColor = true;
            this.pumpSaveButton.Visible = false;
            this.pumpSaveButton.Click += new System.EventHandler(this.pumpSaveButton_Click);
            // 
            // pumpEditButton
            // 
            this.pumpEditButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpEditButton.Location = new System.Drawing.Point(363, 132);
            this.pumpEditButton.Name = "pumpEditButton";
            this.pumpEditButton.Size = new System.Drawing.Size(75, 23);
            this.pumpEditButton.TabIndex = 45;
            this.pumpEditButton.Text = "Edit";
            this.pumpEditButton.UseVisualStyleBackColor = true;
            this.pumpEditButton.Click += new System.EventHandler(this.pumpEditButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(25, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(146, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "Pump was last activated on";
            // 
            // pumpLastActiveControl
            // 
            this.pumpLastActiveControl.Enabled = false;
            this.pumpLastActiveControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpLastActiveControl.Location = new System.Drawing.Point(28, 46);
            this.pumpLastActiveControl.Name = "pumpLastActiveControl";
            this.pumpLastActiveControl.Size = new System.Drawing.Size(207, 22);
            this.pumpLastActiveControl.TabIndex = 0;
            this.pumpLastActiveControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(188, 107);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 13);
            this.label28.TabIndex = 40;
            this.label28.Text = "s";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(81, 107);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(12, 13);
            this.label27.TabIndex = 39;
            this.label27.Text = "s";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pumpActiveForControl
            // 
            this.pumpActiveForControl.Enabled = false;
            this.pumpActiveForControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpActiveForControl.Location = new System.Drawing.Point(28, 105);
            this.pumpActiveForControl.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.pumpActiveForControl.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.pumpActiveForControl.Name = "pumpActiveForControl";
            this.pumpActiveForControl.Size = new System.Drawing.Size(47, 22);
            this.pumpActiveForControl.TabIndex = 32;
            this.pumpActiveForControl.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(128, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(241, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "Pump is inactive before another activation for";
            // 
            // pumpActivatedWaitControl
            // 
            this.pumpActivatedWaitControl.Enabled = false;
            this.pumpActivatedWaitControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpActivatedWaitControl.Location = new System.Drawing.Point(135, 105);
            this.pumpActivatedWaitControl.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.pumpActivatedWaitControl.Name = "pumpActivatedWaitControl";
            this.pumpActivatedWaitControl.Size = new System.Drawing.Size(47, 22);
            this.pumpActivatedWaitControl.TabIndex = 33;
            this.pumpActivatedWaitControl.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(25, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "Pump is active for";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(145, 341);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Terrarium Address";
            // 
            // addressControl
            // 
            this.addressControl.BackColor = System.Drawing.Color.Red;
            this.addressControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addressControl.Location = new System.Drawing.Point(250, 338);
            this.addressControl.Name = "addressControl";
            this.addressControl.Size = new System.Drawing.Size(127, 22);
            this.addressControl.TabIndex = 16;
            this.addressControl.Text = "192.168.100.15";
            this.addressControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(351, 368);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(16, 13);
            this.label26.TabIndex = 33;
            this.label26.Text = "%";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // humiSaveButton
            // 
            this.humiSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.humiSaveButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humiSaveButton.Location = new System.Drawing.Point(380, 365);
            this.humiSaveButton.Name = "humiSaveButton";
            this.humiSaveButton.Size = new System.Drawing.Size(75, 23);
            this.humiSaveButton.TabIndex = 38;
            this.humiSaveButton.Text = "Save";
            this.humiSaveButton.UseVisualStyleBackColor = true;
            this.humiSaveButton.Visible = false;
            this.humiSaveButton.Click += new System.EventHandler(this.humiSaveButton_Click);
            // 
            // humiEditButton
            // 
            this.humiEditButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.humiEditButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humiEditButton.Location = new System.Drawing.Point(461, 365);
            this.humiEditButton.Name = "humiEditButton";
            this.humiEditButton.Size = new System.Drawing.Size(75, 23);
            this.humiEditButton.TabIndex = 37;
            this.humiEditButton.Text = "Edit";
            this.humiEditButton.UseVisualStyleBackColor = true;
            this.humiEditButton.Click += new System.EventHandler(this.humiEditButton_Click);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(214, 368);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(16, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "%";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(236, 368);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Maximum";
            // 
            // humiMaxControl
            // 
            this.humiMaxControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.humiMaxControl.Enabled = false;
            this.humiMaxControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humiMaxControl.Location = new System.Drawing.Point(298, 366);
            this.humiMaxControl.Maximum = new decimal(new int[] {
            101,
            0,
            0,
            0});
            this.humiMaxControl.Name = "humiMaxControl";
            this.humiMaxControl.Size = new System.Drawing.Size(47, 22);
            this.humiMaxControl.TabIndex = 5;
            this.humiMaxControl.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(100, 368);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Minimum";
            // 
            // humiMinControl
            // 
            this.humiMinControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.humiMinControl.Enabled = false;
            this.humiMinControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humiMinControl.Location = new System.Drawing.Point(161, 366);
            this.humiMinControl.Maximum = new decimal(new int[] {
            101,
            0,
            0,
            0});
            this.humiMinControl.Name = "humiMinControl";
            this.humiMinControl.Size = new System.Drawing.Size(47, 22);
            this.humiMinControl.TabIndex = 4;
            this.humiMinControl.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(6, 14);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(530, 346);
            this.panel2.TabIndex = 35;
            // 
            // tempMaxControl
            // 
            this.tempMaxControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tempMaxControl.Enabled = false;
            this.tempMaxControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempMaxControl.Location = new System.Drawing.Point(298, 366);
            this.tempMaxControl.Name = "tempMaxControl";
            this.tempMaxControl.Size = new System.Drawing.Size(47, 22);
            this.tempMaxControl.TabIndex = 5;
            this.tempMaxControl.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(100, 368);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Minimum";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(236, 368);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Maximum";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(214, 368);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(18, 13);
            this.label18.TabIndex = 29;
            this.label18.Text = "°C";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(351, 368);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(18, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "°C";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tempSaveButton
            // 
            this.tempSaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tempSaveButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempSaveButton.Location = new System.Drawing.Point(380, 365);
            this.tempSaveButton.Name = "tempSaveButton";
            this.tempSaveButton.Size = new System.Drawing.Size(75, 23);
            this.tempSaveButton.TabIndex = 36;
            this.tempSaveButton.Text = "Save";
            this.tempSaveButton.UseVisualStyleBackColor = true;
            this.tempSaveButton.Visible = false;
            this.tempSaveButton.Click += new System.EventHandler(this.tempSaveButton_Click);
            // 
            // tempEditButton
            // 
            this.tempEditButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tempEditButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempEditButton.Location = new System.Drawing.Point(461, 365);
            this.tempEditButton.Name = "tempEditButton";
            this.tempEditButton.Size = new System.Drawing.Size(75, 23);
            this.tempEditButton.TabIndex = 35;
            this.tempEditButton.Text = "Edit";
            this.tempEditButton.UseVisualStyleBackColor = true;
            this.tempEditButton.Click += new System.EventHandler(this.tempEditButton_Click);
            // 
            // tempMinControl
            // 
            this.tempMinControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tempMinControl.Enabled = false;
            this.tempMinControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempMinControl.Location = new System.Drawing.Point(161, 366);
            this.tempMinControl.Name = "tempMinControl";
            this.tempMinControl.Size = new System.Drawing.Size(47, 22);
            this.tempMinControl.TabIndex = 4;
            this.tempMinControl.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(6, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(530, 344);
            this.panel1.TabIndex = 34;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(106, 65);
            this.panel3.TabIndex = 36;
            this.panel3.Click += new System.EventHandler(this.panel5_Click);
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(106, 65);
            this.panel4.TabIndex = 37;
            this.panel4.Click += new System.EventHandler(this.panel6_Click);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label15.Location = new System.Drawing.Point(116, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 22);
            this.label15.TabIndex = 38;
            this.label15.Text = "Cool";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label15.Click += new System.EventHandler(this.panel5_Click);
            // 
            // tempValueLabel
            // 
            this.tempValueLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempValueLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.tempValueLabel.Location = new System.Drawing.Point(116, 25);
            this.tempValueLabel.Name = "tempValueLabel";
            this.tempValueLabel.Size = new System.Drawing.Size(99, 22);
            this.tempValueLabel.TabIndex = 39;
            this.tempValueLabel.Text = "24°C";
            this.tempValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tempValueLabel.Click += new System.EventHandler(this.panel5_Click);
            // 
            // humiValueLabel
            // 
            this.humiValueLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humiValueLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.humiValueLabel.Location = new System.Drawing.Point(115, 25);
            this.humiValueLabel.Name = "humiValueLabel";
            this.humiValueLabel.Size = new System.Drawing.Size(99, 22);
            this.humiValueLabel.TabIndex = 41;
            this.humiValueLabel.Text = "33%";
            this.humiValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.humiValueLabel.Click += new System.EventHandler(this.panel6_Click);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label21.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label21.Location = new System.Drawing.Point(115, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 22);
            this.label21.TabIndex = 40;
            this.label21.Text = "Humidity";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.Click += new System.EventHandler(this.panel6_Click);
            // 
            // temperaturePanel
            // 
            this.temperaturePanel.BackColor = System.Drawing.Color.AliceBlue;
            this.temperaturePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.temperaturePanel.Controls.Add(this.tempLoHiLabel);
            this.temperaturePanel.Controls.Add(this.panel3);
            this.temperaturePanel.Controls.Add(this.label15);
            this.temperaturePanel.Controls.Add(this.tempValueLabel);
            this.temperaturePanel.Location = new System.Drawing.Point(6, 13);
            this.temperaturePanel.Name = "temperaturePanel";
            this.temperaturePanel.Size = new System.Drawing.Size(226, 73);
            this.temperaturePanel.TabIndex = 42;
            this.temperaturePanel.Click += new System.EventHandler(this.panel5_Click);
            // 
            // tempLoHiLabel
            // 
            this.tempLoHiLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempLoHiLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.tempLoHiLabel.Location = new System.Drawing.Point(116, 47);
            this.tempLoHiLabel.Name = "tempLoHiLabel";
            this.tempLoHiLabel.Size = new System.Drawing.Size(99, 22);
            this.tempLoHiLabel.TabIndex = 40;
            this.tempLoHiLabel.Text = "20°C to 30°C";
            this.tempLoHiLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tempLoHiLabel.Click += new System.EventHandler(this.panel5_Click);
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.humiLoHiLabel);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.humiValueLabel);
            this.panel6.Location = new System.Drawing.Point(6, 163);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(226, 73);
            this.panel6.TabIndex = 43;
            this.panel6.Click += new System.EventHandler(this.panel6_Click);
            // 
            // humiLoHiLabel
            // 
            this.humiLoHiLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.humiLoHiLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.humiLoHiLabel.Location = new System.Drawing.Point(117, 47);
            this.humiLoHiLabel.Name = "humiLoHiLabel";
            this.humiLoHiLabel.Size = new System.Drawing.Size(99, 22);
            this.humiLoHiLabel.TabIndex = 42;
            this.humiLoHiLabel.Text = "30% to 40%";
            this.humiLoHiLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.humiLoHiLabel.Click += new System.EventHandler(this.panel6_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.temperaturePanel2);
            this.groupBox5.Controls.Add(this.panel9);
            this.groupBox5.Controls.Add(this.panel7);
            this.groupBox5.Controls.Add(this.temperaturePanel);
            this.groupBox5.Controls.Add(this.panel6);
            this.groupBox5.Location = new System.Drawing.Point(12, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(242, 394);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            // 
            // temperaturePanel2
            // 
            this.temperaturePanel2.BackColor = System.Drawing.SystemColors.Window;
            this.temperaturePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.temperaturePanel2.Controls.Add(this.temp2LoHiLabel);
            this.temperaturePanel2.Controls.Add(this.panel12);
            this.temperaturePanel2.Controls.Add(this.label22);
            this.temperaturePanel2.Controls.Add(this.temp2ValueLabel);
            this.temperaturePanel2.Location = new System.Drawing.Point(6, 88);
            this.temperaturePanel2.Name = "temperaturePanel2";
            this.temperaturePanel2.Size = new System.Drawing.Size(226, 73);
            this.temperaturePanel2.TabIndex = 43;
            this.temperaturePanel2.Click += new System.EventHandler(this.temperaturePanel2_Click);
            // 
            // temp2LoHiLabel
            // 
            this.temp2LoHiLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2LoHiLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.temp2LoHiLabel.Location = new System.Drawing.Point(116, 47);
            this.temp2LoHiLabel.Name = "temp2LoHiLabel";
            this.temp2LoHiLabel.Size = new System.Drawing.Size(99, 22);
            this.temp2LoHiLabel.TabIndex = 40;
            this.temp2LoHiLabel.Text = "20°C to 30°C";
            this.temp2LoHiLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.temp2LoHiLabel.Click += new System.EventHandler(this.temperaturePanel2_Click);
            // 
            // panel12
            // 
            this.panel12.Location = new System.Drawing.Point(3, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(106, 65);
            this.panel12.TabIndex = 36;
            this.panel12.Click += new System.EventHandler(this.temperaturePanel2_Click);
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label22.Location = new System.Drawing.Point(116, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 22);
            this.label22.TabIndex = 38;
            this.label22.Text = "Warm";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.Click += new System.EventHandler(this.temperaturePanel2_Click);
            // 
            // temp2ValueLabel
            // 
            this.temp2ValueLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2ValueLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.temp2ValueLabel.Location = new System.Drawing.Point(116, 25);
            this.temp2ValueLabel.Name = "temp2ValueLabel";
            this.temp2ValueLabel.Size = new System.Drawing.Size(99, 22);
            this.temp2ValueLabel.TabIndex = 39;
            this.temp2ValueLabel.Text = "24°C";
            this.temp2ValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.temp2ValueLabel.Click += new System.EventHandler(this.temperaturePanel2_Click);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.label24);
            this.panel9.Location = new System.Drawing.Point(6, 313);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(226, 73);
            this.panel9.TabIndex = 45;
            this.panel9.Click += new System.EventHandler(this.panel9_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.MistyRose;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(106, 65);
            this.panel10.TabIndex = 37;
            this.panel10.Click += new System.EventHandler(this.panel9_Click);
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.ForestGreen;
            this.label24.Location = new System.Drawing.Point(115, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 22);
            this.label24.TabIndex = 41;
            this.label24.Text = "Settings";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.Click += new System.EventHandler(this.panel9_Click);
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.timeFromToLabel);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.timeValueLabel);
            this.panel7.Location = new System.Drawing.Point(6, 238);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(226, 73);
            this.panel7.TabIndex = 44;
            this.panel7.Click += new System.EventHandler(this.panel7_Click);
            // 
            // timeFromToLabel
            // 
            this.timeFromToLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeFromToLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.timeFromToLabel.Location = new System.Drawing.Point(115, 49);
            this.timeFromToLabel.Name = "timeFromToLabel";
            this.timeFromToLabel.Size = new System.Drawing.Size(99, 22);
            this.timeFromToLabel.TabIndex = 42;
            this.timeFromToLabel.Text = "08:00 to 21:00";
            this.timeFromToLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(106, 65);
            this.panel8.TabIndex = 37;
            this.panel8.Click += new System.EventHandler(this.panel7_Click);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label16.Location = new System.Drawing.Point(115, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(99, 22);
            this.label16.TabIndex = 40;
            this.label16.Text = "Time";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label16.Click += new System.EventHandler(this.panel7_Click);
            // 
            // timeValueLabel
            // 
            this.timeValueLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeValueLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.timeValueLabel.Location = new System.Drawing.Point(115, 26);
            this.timeValueLabel.Name = "timeValueLabel";
            this.timeValueLabel.Size = new System.Drawing.Size(99, 22);
            this.timeValueLabel.TabIndex = 41;
            this.timeValueLabel.Text = "15:47";
            this.timeValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.timeValueLabel.Click += new System.EventHandler(this.panel7_Click);
            // 
            // tempGroup
            // 
            this.tempGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tempGroup.Controls.Add(this.tempEditButton);
            this.tempGroup.Controls.Add(this.tempSaveButton);
            this.tempGroup.Controls.Add(this.panel1);
            this.tempGroup.Controls.Add(this.label2);
            this.tempGroup.Controls.Add(this.label19);
            this.tempGroup.Controls.Add(this.tempMinControl);
            this.tempGroup.Controls.Add(this.label18);
            this.tempGroup.Controls.Add(this.tempMaxControl);
            this.tempGroup.Controls.Add(this.label3);
            this.tempGroup.Location = new System.Drawing.Point(260, 4);
            this.tempGroup.Name = "tempGroup";
            this.tempGroup.Size = new System.Drawing.Size(542, 394);
            this.tempGroup.TabIndex = 0;
            this.tempGroup.TabStop = false;
            // 
            // humiGroup
            // 
            this.humiGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.humiGroup.Controls.Add(this.humiSaveButton);
            this.humiGroup.Controls.Add(this.label26);
            this.humiGroup.Controls.Add(this.humiEditButton);
            this.humiGroup.Controls.Add(this.panel2);
            this.humiGroup.Controls.Add(this.label25);
            this.humiGroup.Controls.Add(this.label4);
            this.humiGroup.Controls.Add(this.humiMinControl);
            this.humiGroup.Controls.Add(this.humiMaxControl);
            this.humiGroup.Controls.Add(this.label1);
            this.humiGroup.Location = new System.Drawing.Point(260, 4);
            this.humiGroup.Name = "humiGroup";
            this.humiGroup.Size = new System.Drawing.Size(542, 394);
            this.humiGroup.TabIndex = 36;
            this.humiGroup.TabStop = false;
            this.humiGroup.Visible = false;
            // 
            // temp2Group
            // 
            this.temp2Group.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.temp2Group.Controls.Add(this.temp2EditButton);
            this.temp2Group.Controls.Add(this.temp2SaveButton);
            this.temp2Group.Controls.Add(this.panel11);
            this.temp2Group.Controls.Add(this.label20);
            this.temp2Group.Controls.Add(this.label23);
            this.temp2Group.Controls.Add(this.temp2MinControl);
            this.temp2Group.Controls.Add(this.label29);
            this.temp2Group.Controls.Add(this.temp2MaxControl);
            this.temp2Group.Controls.Add(this.label30);
            this.temp2Group.Location = new System.Drawing.Point(260, 4);
            this.temp2Group.Name = "temp2Group";
            this.temp2Group.Size = new System.Drawing.Size(542, 394);
            this.temp2Group.TabIndex = 37;
            this.temp2Group.TabStop = false;
            // 
            // temp2EditButton
            // 
            this.temp2EditButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.temp2EditButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2EditButton.Location = new System.Drawing.Point(461, 365);
            this.temp2EditButton.Name = "temp2EditButton";
            this.temp2EditButton.Size = new System.Drawing.Size(75, 23);
            this.temp2EditButton.TabIndex = 35;
            this.temp2EditButton.Text = "Edit";
            this.temp2EditButton.UseVisualStyleBackColor = true;
            this.temp2EditButton.Click += new System.EventHandler(this.temp2EditButton_Click);
            // 
            // temp2SaveButton
            // 
            this.temp2SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.temp2SaveButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2SaveButton.Location = new System.Drawing.Point(380, 365);
            this.temp2SaveButton.Name = "temp2SaveButton";
            this.temp2SaveButton.Size = new System.Drawing.Size(75, 23);
            this.temp2SaveButton.TabIndex = 36;
            this.temp2SaveButton.Text = "Save";
            this.temp2SaveButton.UseVisualStyleBackColor = true;
            this.temp2SaveButton.Visible = false;
            this.temp2SaveButton.Click += new System.EventHandler(this.temp2SaveButton_Click);
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.Location = new System.Drawing.Point(6, 13);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(530, 344);
            this.panel11.TabIndex = 34;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(100, 368);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Minimum";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(351, 368);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(18, 13);
            this.label23.TabIndex = 30;
            this.label23.Text = "°C";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // temp2MinControl
            // 
            this.temp2MinControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.temp2MinControl.Enabled = false;
            this.temp2MinControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2MinControl.Location = new System.Drawing.Point(161, 366);
            this.temp2MinControl.Name = "temp2MinControl";
            this.temp2MinControl.Size = new System.Drawing.Size(47, 22);
            this.temp2MinControl.TabIndex = 4;
            this.temp2MinControl.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(214, 368);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(18, 13);
            this.label29.TabIndex = 29;
            this.label29.Text = "°C";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // temp2MaxControl
            // 
            this.temp2MaxControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.temp2MaxControl.Enabled = false;
            this.temp2MaxControl.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temp2MaxControl.Location = new System.Drawing.Point(298, 366);
            this.temp2MaxControl.Name = "temp2MaxControl";
            this.temp2MaxControl.Size = new System.Drawing.Size(47, 22);
            this.temp2MaxControl.TabIndex = 5;
            this.temp2MaxControl.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(236, 368);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 25;
            this.label30.Text = "Maximum";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(814, 409);
            this.Controls.Add(this.timeGroup);
            this.Controls.Add(this.temp2Group);
            this.Controls.Add(this.humiGroup);
            this.Controls.Add(this.otherGroup);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.tempGroup);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Terrarium GUI";
            this.timeGroup.ResumeLayout(false);
            this.timeGroup.PerformLayout();
            this.otherGroup.ResumeLayout(false);
            this.otherGroup.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateHistoryControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateSensorsControl)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pumpActiveForControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpActivatedWaitControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humiMaxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humiMinControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempMaxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempMinControl)).EndInit();
            this.temperaturePanel.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.temperaturePanel2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tempGroup.ResumeLayout(false);
            this.tempGroup.PerformLayout();
            this.humiGroup.ResumeLayout(false);
            this.humiGroup.PerformLayout();
            this.temp2Group.ResumeLayout(false);
            this.temp2Group.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.temp2MinControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.temp2MaxControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox timeGroup;
        private System.Windows.Forms.GroupBox otherGroup;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox timeTimeControl;
        private System.Windows.Forms.TextBox timeDayEndControl;
        private System.Windows.Forms.TextBox timeDayStartControl;
        private System.Windows.Forms.DateTimePicker timeDateControl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown humiMaxControl;
        private System.Windows.Forms.NumericUpDown humiMinControl;
        private System.Windows.Forms.TextBox addressControl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown pumpActivatedWaitControl;
        private System.Windows.Forms.NumericUpDown pumpActiveForControl;
        private System.Windows.Forms.TextBox pumpLastActiveControl;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown tempMaxControl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown tempMinControl;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button tempEditButton;
        private System.Windows.Forms.Button tempSaveButton;
        private System.Windows.Forms.Button humiSaveButton;
        private System.Windows.Forms.Button humiEditButton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label tempValueLabel;
        private System.Windows.Forms.Label humiValueLabel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel temperaturePanel;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox tempGroup;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label timeValueLabel;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox humiGroup;
        private System.Windows.Forms.Label tempLoHiLabel;
        private System.Windows.Forms.Label humiLoHiLabel;
        private System.Windows.Forms.Label timeFromToLabel;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button timeSaveButton;
        private System.Windows.Forms.Button timeEditButton;
        private System.Windows.Forms.Button timeNowButton;
        private System.Windows.Forms.Button updateSaveButton;
        private System.Windows.Forms.Button updateEditButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button pumpSaveButton;
        private System.Windows.Forms.Button pumpEditButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown updateHistoryControl;
        private System.Windows.Forms.NumericUpDown updateSensorsControl;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel temperaturePanel2;
        private System.Windows.Forms.Label temp2LoHiLabel;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label temp2ValueLabel;
        private System.Windows.Forms.GroupBox temp2Group;
        private System.Windows.Forms.Button temp2EditButton;
        private System.Windows.Forms.Button temp2SaveButton;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown temp2MinControl;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown temp2MaxControl;
        private System.Windows.Forms.Label label30;


    }
}

