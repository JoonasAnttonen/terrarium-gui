﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Web.Script.Serialization;

namespace Terrarium_GUI
{
    public struct GetTempResponse
    {
        public Int32 Low0 { get; set; }
        public Int32 Low1 { get; set; }
        public Int32 High0 { get; set; }
        public Int32 High1 { get; set; }
        public Int32[][] Values { get; set; }
    }

    public struct GetHumiResponse
    {
        public Int32 Low { get; set; }
        public Int32 High { get; set; }
        public Int32[][] Values { get; set; }
    }

    public struct GetTimeResponse
    {
        public uint Time { get; set; }
    }

    public struct GetDayResponse
    {
        public int StartOfDay { get; set; }
        public int EndOfDay { get; set; }
    }

    public struct GetUpdateResponse
    {
        public int History { get; set; }
        public int Sensor { get; set; }
    }

    public struct GetPumpResponse
    {
        public uint ActivatedLast { get; set; }
        public int ActiveFor { get; set; }
        public int ActivatedWait { get; set; }
    }

    public struct TerrariumResponse
    {
        public Boolean OK { get; private set; }
        public String Data { get; private set; }

        public TerrariumResponse( Boolean ok, String data )
            : this()
        {
            OK = ok;
            Data = data;
        }

        public T Deserialize<T>()
        {
            return new JavaScriptSerializer().Deserialize<T>( Data );
        }
    }

    public class TerrariumRequest
    {
        List<Tuple<string, string>> parameters = new List<Tuple<string, string>>();
        string address;

        public static TerrariumResponse Do( string address, string method ) { return Do( address, method, new string[ 0 ] ); }
        public static TerrariumResponse Do( string address, string method, params string[] parameters )
        {
            var request = new TerrariumRequest( address, method );

            System.Diagnostics.Debug.Assert( parameters.Length % 2 == 0, "Not all parameters have the form Key=Value" );

            for ( var i = 0; i < parameters.Length; i++ )
            {
                request.AddParameter( parameters[ i++ ], parameters[ i ] );
            }

            return request.Execute();
        }

        public TerrariumRequest( string address, string method )
        {
            this.address = address;
            parameters.Add( new Tuple<string, string>( "Method", method ) );
        }

        public void AddParameter( string name, string value )
        {
            parameters.Add( new Tuple<string, string>( name, value ) );
        }

        static byte[] BuildQuery( List<Tuple<string, string>> parameters )
        {
            var builder = new StringBuilder();

            // Build the query url string.
            foreach ( var parameter in parameters )
                builder.AppendFormat( "{0}={1}&", Uri.EscapeDataString( parameter.Item1 ), Uri.EscapeDataString( parameter.Item2 ) );

            // Remove the trailing "&".
            builder.Length -= 1;

            // Convert the query to a byte array.
            return Encoding.UTF8.GetBytes( builder.ToString() );
        }

        public TerrariumResponse Execute()
        {
            var requestData = BuildQuery( parameters );
            var request = WebRequest.Create( address ) as HttpWebRequest;
            request.Timeout = 3000;
            request.ServicePoint.Expect100Continue = false;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = requestData.Length;
            request.UserAgent = "Terrarium GUI";
            request.Method = "POST";
            request.KeepAlive = false;

            // Write our request data.
            using ( var writeStream = request.GetRequestStream() )
            {
                writeStream.Write( requestData, 0, requestData.Length );
                writeStream.Flush();
            }

            HttpWebResponse webresponse;
            var ok = false;
            var response = String.Empty;
            
            try
            {
                webresponse = (HttpWebResponse)request.GetResponse();
                ok = true;
            }
            catch ( WebException e )
            {
                webresponse = (HttpWebResponse)e.Response;
                ok = false;
            }

            if ( webresponse != null )
            {
                using ( var reader = new System.IO.StreamReader( webresponse.GetResponseStream() ) )
                {
                    response = reader.ReadToEnd();
                }
            }
            else
            {
                response = String.Empty;
            }

            return new TerrariumResponse( ok, response );
        }
    }

    public struct GetStateResponse
    {
        public int GetTemp { get; set; }
        public int GetHumi { get; set; }
    }

    public class TerrariumInfo
    {
        public GetStateResponse GetState { get; set; }
        public GetTempResponse GetTemp { get; set; }
        public GetHumiResponse GetHumi { get; set; }
        public GetTimeResponse GetTime { get; set; }
        public GetPumpResponse GetPump { get; set; }
        public GetDayResponse GetDay { get; set; }
        public GetUpdateResponse GetUpdateParams { get; set; }
    }

    public static class TerrariumAPI
    {
        // -----------------------------------------------------------
        // Time utilities

        /// <summary>
        /// Convert a <see cref="DateTime"/> to UNIX time.
        /// </summary>
        /// <param name="time"><see cref="DateTime"/> to convert.</param>
        public static Int64 DateTimeToUNIXTime( DateTime time )
        {
            return (Int64)((time.Subtract( new DateTime( 1970, 1, 1 ) )).TotalSeconds);
        }

        /// <summary>
        /// Convert UNIX time to a <see cref="DateTime"/>.
        /// </summary>
        /// <param name="unixTime">The UNIX time to convert.</param>
        public static DateTime UNIXTimeToDateTime( Int64 unixTime )
        {
            return new DateTime( 1970, 1, 1 ).AddSeconds( unixTime );
        }

        public static Boolean GetInfo( String address, out TerrariumInfo result )
        {
            try
            {
                var response = TerrariumRequest.Do( address, "GetAll" );
                result = (response.OK ? response.Deserialize<TerrariumInfo>() : new TerrariumInfo());
                return response.OK;
            }
            catch ( Exception )
            {
                result = new TerrariumInfo();
                return false;
            }
        }

        public static Boolean GetTime( String address, out GetTimeResponse result )
        {
            try
            {
                var response = TerrariumRequest.Do( address, "GetTime" );
                result = (response.OK ? response.Deserialize<GetTimeResponse>() : new GetTimeResponse());
                return response.OK;
            }
            catch ( Exception )
            {
                result = new GetTimeResponse();
                return false;
            }
        }

        public static TerrariumResponse SetTime( String address, Int64 unixTime )
        {
            try
            {
                var response = TerrariumRequest.Do( address, "SetTime", "Time", unixTime.ToString() );
                return response;
            }
            catch ( Exception )
            {
                return new TerrariumResponse();
            }
        }

        public static Task<TerrariumResponse> SetTimeAsync( String address, Int64 unixTime )
        {
            return Task<TerrariumResponse>.Run( () => TerrariumAPI.SetTime( address, unixTime ) );
        }

        public static GetTempResponse GetTemp( String address )
        {
            return TerrariumRequest.Do( address,
                "GetTemp" )
                .Deserialize<GetTempResponse>();
        }

        public static TerrariumResponse SetTemp( String address, Int32 low0, Int32 low1, Int32 high0, Int32 high1 )
        {
            try
            {
                var response = TerrariumRequest.Do( address, 
                    "SetTempParams", 
                    "Low0", low0.ToString(), 
                    "Low1", low1.ToString(), 
                    "High0", high0.ToString(),
                    "High1", high1.ToString());
                return response;
            }
            catch ( Exception )
            {
                return new TerrariumResponse();
            }
        }

        public static Task<TerrariumResponse> SetTempAsync( String address, Int32 low0, Int32 low1, Int32 high0, Int32 high1 )
        {
            return Task<TerrariumResponse>.Run( () => TerrariumAPI.SetTemp( address, low0, low1, high0, high1 ) );
        }

        public static GetHumiResponse GetHumi( String address )
        {
            return TerrariumRequest.Do( address,
                "GetHumi" )
                .Deserialize<GetHumiResponse>();
        }

        public static TerrariumResponse SetHumi( String address, Int32 low, Int32 high )
        {
            return TerrariumRequest.Do( address,
                "SetHumiParams",
                "Low", low.ToString(),
                "High", high.ToString() );
        }

        public static Task<TerrariumResponse> SetHumiAsync( String address, Int32 min, Int32 max )
        {
            return Task<TerrariumResponse>.Run( () => TerrariumAPI.SetHumi( address, min, max ) );
        }

        public static GetDayResponse GetDay( String address )
        {
            return TerrariumRequest.Do( address,
                "GetDay" )
                .Deserialize<GetDayResponse>();
        }

        public static TerrariumResponse SetDay( String address, Int32 startOfDay, Int32 endOfDay )
        {
            try
            {
                var response = TerrariumRequest.Do( address, "SetDayParams", "StartOfDay", startOfDay.ToString(), "EndOfDay", endOfDay.ToString() );
                return response;
            }
            catch ( Exception )
            {
                return new TerrariumResponse();
            }
        }

        public static Task<TerrariumResponse> SetDayAsync( String address, Int32 startOfDay, Int32 endOfDay )
        {
            return Task<TerrariumResponse>.Run( () => TerrariumAPI.SetDay( address, startOfDay, endOfDay ) );
        }

        public static GetPumpResponse GetPump( String address )
        {
            return TerrariumRequest.Do( address,
                "GetPump" )
                .Deserialize<GetPumpResponse>();
        }

        public static TerrariumResponse SetPump( String address, Int32 activeFor, Int32 activatedWait )
        {
            try
            {
                var response = TerrariumRequest.Do( address, "SetPumpParams", "ActiveFor", activeFor.ToString(), "ActivatedWait", activatedWait.ToString() );
                return response;
            }
            catch ( Exception )
            {
                return new TerrariumResponse();
            }
        }

        public static Task<TerrariumResponse> SetPumpAsync( String address, Int32 activeFor, Int32 activatedWait )
        {
            return Task<TerrariumResponse>.Run( () => TerrariumAPI.SetPump( address, activeFor, activatedWait ) );
        }

        public static GetUpdateResponse GetUpdates( String address )
        {
            return TerrariumRequest.Do( address,
                "GetPump" )
                .Deserialize<GetUpdateResponse>();
        }

        public static TerrariumResponse SetUpdates( String address, Int32 history, Int32 sensor )
        {
            try
            {
                var response = TerrariumRequest.Do( address, "SetUpdateParams", "History", history.ToString(), "Sensor", sensor.ToString() );
                return response;
            }
            catch ( Exception )
            {
                return new TerrariumResponse();
            }
        }

        public static Task<TerrariumResponse> SetUpdatesAsync( String address, Int32 history, Int32 sensor )
        {
            return Task<TerrariumResponse>.Run( () => TerrariumAPI.SetUpdates( address, history, sensor ) );
        }
    }
}
