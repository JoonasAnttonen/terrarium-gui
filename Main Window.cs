﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

using System.Threading;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace Terrarium_GUI
{
    public partial class MainWindow : Form
    {
        static readonly Object Source = new Object();

        BackgroundWorker updater = new BackgroundWorker();

        List<DataPoint> tempSource = new List<DataPoint>();
        List<DataPoint> temp2Source = new List<DataPoint>();
        List<DataPoint> humiSource = new List<DataPoint>();

        PlotView tempPlot = new PlotView();
        PlotView temp2Plot = new PlotView();
        PlotView humiPlot = new PlotView();
        PlotView timePlot = new PlotView();

        PlotView tempPlotMini = new PlotView();
        PlotView temp2PlotMini = new PlotView();
        PlotView humiPlotMini = new PlotView();
        PlotView timePlotSmall = new PlotView();

        LineSeries timeSeries;
        LineSeries timeSeriesSmall;
        AreaSeries timeAreaSeries;
        AreaSeries timeAreaSeriesSmall;

        LineSeries tempSeries;
        LineSeries tempSeriesSmall;
        LineSeries temp2Series;
        LineSeries temp2SeriesSmall;
        LineSeries humiSeries;
        LineSeries humiSeriesSmall;
        AreaSeries tempAreaSeries;
        AreaSeries temp2AreaSeries;
        AreaSeries humiAreaSeries;
        AreaSeries tempAreaMiniSeries;
        AreaSeries temp2AreaMiniSeries;
        AreaSeries humiAreaMiniSeries;

        Int32 updateInterval = (Int32)TimeSpan.FromSeconds( 3 ).TotalMilliseconds;
        Int32 lastUpdate = 0;
        AutoResetEvent updateEvent = new AutoResetEvent( false );

        bool tempEditing = false;
        bool temp2Editing = false;
        bool humiEditing = false;
        bool timeEditing = false;
        bool pumpEditing = false;
        bool updateEditing = false;
        int tempMinStore = 0;
        int tempMaxStore = 0;
        int temp2MinStore = 0;
        int temp2MaxStore = 0;
        int humiMinStore = 0;
        int humiMaxStore = 0;
        DateTime timeDateStore;
        string timeTimeStore;
        string timeDayStartStore;
        string timeDayEndStore;
        int pumpActiveForStore = 0;
        int pumpActivatedWaitStore = 0;
        int updateHistoryStore = 2;
        int updateSensorsStore = 2;

        Boolean CheckRequestResult( TerrariumResponse response )
        {
            return CheckRequestResult( response.OK );
        }

        Boolean CheckRequestResult( Boolean result )
        {
            this.Invoke( (Action)(() =>
            {
                if ( result )
                {
                    addressControl.BackColor = System.Drawing.Color.LightGreen;
                }
                else
                {
                    addressControl.BackColor = System.Drawing.Color.Red;
                }
            }) );

            return result;
        }

        DateTimeAxis MakeXAxis()
        {
            var axis = new DateTimeAxis();
            axis.FontSize = this.Font.Size;
            axis.Position = AxisPosition.Bottom;
            axis.TickStyle = OxyPlot.Axes.TickStyle.None;
            axis.MinorTickSize = 1;
            axis.MinimumPadding = -0.01;
            axis.MaximumPadding = -0.01;
            axis.MajorGridlineStyle = LineStyle.Solid;
            return axis;
        }

        LinearAxis MakeYAxis()
        {
            var axis = new LinearAxis();
            axis.FontSize = this.Font.Size;
            axis.TickStyle = OxyPlot.Axes.TickStyle.None;
            axis.MinorTickSize = 1;
            axis.MajorStep = 5;
            axis.MinimumPadding = 0.1;
            axis.MaximumPadding = 0.1;
            axis.MinorGridlineStyle = LineStyle.Solid;
            axis.MajorGridlineStyle = LineStyle.Solid;
            axis.Position = AxisPosition.Left;
            return axis;
        }

        DateTimeAxis MakeMiniXAxis()
        {
            var axis = new DateTimeAxis();
            axis.Position = AxisPosition.Bottom;
            axis.TickStyle = OxyPlot.Axes.TickStyle.None;
            axis.MinorTickSize = 1;
            axis.IsAxisVisible = false;
            axis.MinimumPadding = -0.01;
            axis.MaximumPadding = -0.01;
            return axis;
        }

        LinearAxis MakeMiniYAxis()
        {
            var axis = new LinearAxis();
            axis.TickStyle = OxyPlot.Axes.TickStyle.None;
            axis.MinimumPadding = 0.1;
            axis.MaximumPadding = 1.0;
            axis.IsAxisVisible = false;
            axis.MajorStep = 5;
            axis.Position = AxisPosition.Left;
            return axis;
        }

        LineSeries MakeDataSeries( OxyColor color )
        {
            var series = new LineSeries();
            series.Color = color;
            series.MarkerStroke = color;
            series.Smooth = true;
            series.MarkerType = MarkerType.Circle;
            return series;
        }

        LineSeries MakeDataSeriesSmall( OxyColor color )
        {
            var series = new LineSeries();
            series.StrokeThickness = 1;
            series.Color = color;
            series.TrackerFormatString = String.Empty;
            series.Smooth = true;
            return series;
        }

        AreaSeries MakeToleranceSeries( OxyColor color )
        {
            var series = new AreaSeries();
            series.Title = "Tolerance";
            series.TrackerFormatString = "";
            series.Fill = OxyColor.FromAColor( 50, color );
            series.Color = OxyColor.FromAColor( 100, color );
            return series;
        }

        AreaSeries MakeToleranceSeriesSmall( OxyColor color )
        {
            var series = new AreaSeries();
            series.TrackerFormatString = "";
            series.StrokeThickness = 1;
            series.Fill = OxyColor.FromAColor( 50, color );
            series.Color = color;
            return series;
        }

        PlotModel MakeModel( OxyColor color )
        {
            var model = new PlotModel();
            model.TitleFontWeight = 500;
            model.PlotAreaBorderColor = color;
            model.Axes.Add( MakeXAxis() );
            model.Axes.Add( MakeYAxis() );
            return model;
        }

        PlotModel MakeMiniModel( OxyColor color )
        {
            var model = new PlotModel();
            model.PlotAreaBorderColor = color;
            model.Padding = new OxyThickness( 1 );
            model.Axes.Add( MakeMiniXAxis() );
            model.Axes.Add( MakeMiniYAxis() );
            return model;
        }

        LinearAxis MakeTimeYAxis()
        {
            var axis = new LinearAxis();
            axis.Position = AxisPosition.Left;
            axis.IsAxisVisible = false;
            return axis;
        }

        TimeSpanAxis MakeTimeXAxis()
        {
            var axis = new TimeSpanAxis();
            axis.Position = AxisPosition.Bottom;
            axis.MinorTickSize = 0;
            axis.StringFormat = "hh";
            axis.MajorStep = TimeSpanAxis.ToDouble( TimeSpan.FromHours( 1 ) ); ;
            axis.Minimum = TimeSpanAxis.ToDouble( TimeSpan.Parse( "00:00" ) );
            axis.Maximum = TimeSpanAxis.ToDouble( TimeSpan.Parse( "23:59:59" ) );
            axis.FontSize = this.Font.Size;
            return axis;
        }

        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );

            updater.DoWork += updater_DoWork;
            updater.RunWorkerAsync();
        }

        public MainWindow()
        {
            InitializeComponent();

            panel1.Controls.Add( tempPlot );
            panel11.Controls.Add( temp2Plot );
            panel2.Controls.Add( humiPlot );
            panel3.Controls.Add( tempPlotMini );
            panel12.Controls.Add( temp2PlotMini );
            panel4.Controls.Add( humiPlotMini );
            panel5.Controls.Add( timePlot );
            panel8.Controls.Add( timePlotSmall );
            tempPlot.Dock = DockStyle.Fill;
            tempPlotMini.Dock = DockStyle.Fill;
            temp2Plot.Dock = DockStyle.Fill;
            temp2PlotMini.Dock = DockStyle.Fill;
            humiPlot.Dock = DockStyle.Fill;
            humiPlotMini.Dock = DockStyle.Fill;
            timePlot.Dock = DockStyle.Fill;
            timePlotSmall.Dock = DockStyle.Fill;
            tempPlot.Enabled = false;
            tempPlotMini.Enabled = false;
            temp2Plot.Enabled = false;
            temp2PlotMini.Enabled = false;
            humiPlot.Enabled = false;
            humiPlotMini.Enabled = false;
            timePlot.Enabled = false;
            timePlotSmall.Enabled = false;

            timeAreaSeriesSmall = new AreaSeries();
            timeAreaSeriesSmall.Color = OxyColors.SteelBlue;
            timeAreaSeriesSmall.Color2 = OxyColors.SteelBlue;

            var timeXSmall = MakeTimeXAxis();
            timeXSmall.IsAxisVisible = false;

            timeAreaSeries = new AreaSeries();
            timeAreaSeries.Title = "Daytime";
            timeAreaSeries.Color = OxyColors.SteelBlue;
            timeAreaSeries.Color2 = OxyColors.SteelBlue;

            timeSeriesSmall = new LineSeries();
            timeSeriesSmall.Color = OxyColors.DeepSkyBlue;

            timeSeries = new LineSeries();
            timeSeries.Title = "Now";
            timeSeries.StrokeThickness = 4;
            timeSeries.Color = OxyColors.DeepSkyBlue;

            timePlot.Model = new PlotModel();
            timePlot.Model.Title = "Time";
            timePlot.Model.TitleFontWeight = 500;
            timePlot.Model.Axes.Add( MakeTimeYAxis() );
            timePlot.Model.Axes.Add( MakeTimeXAxis() );
            timePlot.Model.Series.Add( timeSeries );
            timePlot.Model.Series.Add( timeAreaSeries );
            timePlotSmall.Model = new PlotModel();
            timePlotSmall.Model.Axes.Add( MakeTimeYAxis() );
            timePlotSmall.Model.Axes.Add( timeXSmall );
            timePlotSmall.Model.Series.Add( timeSeriesSmall );
            timePlotSmall.Model.Series.Add( timeAreaSeriesSmall );

            tempPlotMini.Model = MakeMiniModel( OxyColors.Green );
            temp2PlotMini.Model = MakeMiniModel( OxyColors.Green );
            humiPlotMini.Model = MakeMiniModel( OxyColors.Orange );

            tempPlot.Model = MakeModel( OxyColors.Green );
            tempPlot.Model.Title = "Temperature of the cool side";
            temp2Plot.Model = MakeModel( OxyColors.Green );
            temp2Plot.Model.Title = "Temperature of the warm side";

            humiPlot.Model = MakeModel( OxyColors.Orange );
            humiPlot.Model.Title = "Air humidity";

            tempSeries = MakeDataSeries( OxyColors.Green );
            tempSeries.ItemsSource = tempSource;
            tempSeries.Title = "Temperature";
            tempSeries.TrackerFormatString = "{2:G}\n{4:0}°C";
            temp2Series = MakeDataSeries( OxyColors.Green );
            temp2Series.ItemsSource = tempSource;
            temp2Series.Title = "Temperature";
            temp2Series.TrackerFormatString = "{2:G}\n{4:0}°C";

            tempAreaSeries = MakeToleranceSeries( OxyColors.Green );
            temp2AreaSeries = MakeToleranceSeries( OxyColors.Green );

            humiSeries = MakeDataSeries( OxyColors.Orange );
            humiSeries.ItemsSource = humiSource;
            humiSeries.Title = "Humidity";
            humiSeries.TrackerFormatString = "{2:G}\n{4:0}%";

            humiAreaSeries = MakeToleranceSeries( OxyColors.Orange );

            tempPlot.Model.Series.Add( tempSeries );
            tempPlot.Model.Series.Add( tempAreaSeries );
            temp2Plot.Model.Series.Add( temp2Series );
            temp2Plot.Model.Series.Add( temp2AreaSeries );
            humiPlot.Model.Series.Add( humiSeries );
            humiPlot.Model.Series.Add( humiAreaSeries );

            tempSeriesSmall = MakeDataSeriesSmall( OxyColors.Green );
            tempSeriesSmall.ItemsSource = temp2Source;
            temp2SeriesSmall = MakeDataSeriesSmall( OxyColors.Green );
            temp2SeriesSmall.ItemsSource = temp2Source;
            humiSeriesSmall = MakeDataSeriesSmall( OxyColors.Orange );
            humiSeriesSmall.ItemsSource = humiSource;

            tempAreaMiniSeries = MakeToleranceSeriesSmall( OxyColors.Green );
            temp2AreaMiniSeries = MakeToleranceSeriesSmall( OxyColors.Green );
            humiAreaMiniSeries = MakeToleranceSeriesSmall( OxyColors.Orange );

            tempPlotMini.Model.Series.Add( tempSeriesSmall );
            tempPlotMini.Model.Series.Add( tempAreaMiniSeries );
            temp2PlotMini.Model.Series.Add( temp2SeriesSmall );
            temp2PlotMini.Model.Series.Add( temp2AreaMiniSeries );
            humiPlotMini.Model.Series.Add( humiSeriesSmall );
            humiPlotMini.Model.Series.Add( humiAreaMiniSeries );
        }

        void UpdateUIControls( TerrariumInfo info )
        {
            var tempData = info.GetTemp;
            var humiData = info.GetHumi;
            var timeData = info.GetTime;
            var dayData = info.GetDay;
            var updateData = info.GetUpdateParams;
            var pumpData = info.GetPump;
            var stateData = info.GetState;

            tempPlot.Model.InvalidatePlot( true );
            tempPlotMini.Model.InvalidatePlot( true );
            temp2Plot.Model.InvalidatePlot( true );
            temp2PlotMini.Model.InvalidatePlot( true );
            humiPlot.Model.InvalidatePlot( true );
            humiPlotMini.Model.InvalidatePlot( true );
            timePlot.Model.InvalidatePlot( true );
            timePlotSmall.Model.InvalidatePlot( true );

            tempValueLabel.Text = String.Format( "{0}°C", stateData.GetTemp );
            temp2ValueLabel.Text = String.Format( "{0}°C", tempData.Values[ tempData.Values.Length - 1 ][ 2 ] );
            tempLoHiLabel.Text = String.Format( "{0}°C to {1}°C", tempData.Low0, tempData.High0 );
            temp2LoHiLabel.Text = String.Format( "{0}°C to {1}°C", tempData.Low1, tempData.High1 );
            humiValueLabel.Text = String.Format( "{0}%", stateData.GetHumi );
            humiLoHiLabel.Text = String.Format( "{0}% to {1}%", humiData.Low, humiData.High );
            timeValueLabel.Text = String.Format( "{0:hh\\:mm}", TerrariumAPI.UNIXTimeToDateTime( timeData.Time ).TimeOfDay );
            timeFromToLabel.Text = String.Format( "{0:hh\\:mm} to {1:hh\\:mm}", new TimeSpan( 0, 0, dayData.StartOfDay ), new TimeSpan( 0, 0, dayData.EndOfDay ) );
            pumpLastActiveControl.Text = TerrariumAPI.UNIXTimeToDateTime( pumpData.ActivatedLast ).ToString();

            if ( !tempEditing )
            {
                tempMinStore = tempData.Low0;
                tempMaxStore = tempData.High0;
                tempMinControl.Value = tempMinStore;
                tempMaxControl.Value = tempMaxStore;
            }
            if ( !temp2Editing )
            {
                temp2MinStore = tempData.Low1;
                temp2MaxStore = tempData.High1;
                temp2MinControl.Value = temp2MinStore;
                temp2MaxControl.Value = temp2MaxStore;
            }
            if ( !humiEditing )
            {
                humiMinStore = humiData.Low;
                humiMaxStore = humiData.High;
                humiMinControl.Value = humiMinStore;
                humiMaxControl.Value = humiMaxStore;
            }
            if ( !timeEditing )
            {
                timeDateStore = TerrariumAPI.UNIXTimeToDateTime( timeData.Time ).Date;
                timeTimeStore = TerrariumAPI.UNIXTimeToDateTime( timeData.Time ).TimeOfDay.ToString( "hh\\:mm" );
                timeDayStartStore = new TimeSpan( 0, 0, dayData.StartOfDay ).ToString( "hh\\:mm" );
                timeDayEndStore = new TimeSpan( 0, 0, dayData.EndOfDay ).ToString( "hh\\:mm" );
                timeDateControl.Value = timeDateStore;
                timeTimeControl.Text = timeTimeStore;
                timeDayStartControl.Text = timeDayStartStore;
                timeDayEndControl.Text = timeDayEndStore;
            }
            if ( !pumpEditing )
            {
                pumpActiveForStore = pumpData.ActiveFor;
                pumpActivatedWaitStore = pumpData.ActivatedWait;
                pumpActiveForControl.Value = pumpActiveForStore;
                pumpActivatedWaitControl.Value = pumpActivatedWaitStore;
            }
            if ( !updateEditing )
            {
                updateHistoryStore = updateData.History;
                updateSensorsStore = updateData.Sensor;
                updateHistoryControl.Value = updateHistoryStore;
                updateSensorsControl.Value = updateSensorsStore;
            }
        }

        void updater_DoWork( object sender, DoWorkEventArgs e )
        {
            Interlocked.Exchange( ref lastUpdate, Environment.TickCount - updateInterval );

            while ( !e.Cancel )
            {
                if ( (Environment.TickCount - lastUpdate) < updateInterval )
                {
                    Thread.Sleep( 0 );
                    continue;
                }

                TerrariumInfo info;
                var res = TerrariumAPI.GetInfo( GetAddress(), out info );

                Interlocked.Exchange( ref lastUpdate, Environment.TickCount );

                CheckRequestResult( res );

                if ( !res )
                    continue;

                var tempData = info.GetTemp;
                var humiData = info.GetHumi;
                var timeData = info.GetTime;
                var dayData = info.GetDay;
                var stateData = info.GetState;

                lock ( Source )
                {
                    timeSeries.Points.Clear();
                    timeSeriesSmall.Points.Clear();

                    var currentTimePoint = TimeSpanAxis.ToDouble( TerrariumAPI.UNIXTimeToDateTime( timeData.Time ).TimeOfDay );
                    timeSeries.Points.Add( new DataPoint( currentTimePoint, 0 ) );
                    timeSeries.Points.Add( new DataPoint( currentTimePoint, 1 ) );
                    timeSeriesSmall.Points.Add( new DataPoint( currentTimePoint, 0 ) );
                    timeSeriesSmall.Points.Add( new DataPoint( currentTimePoint, 1 ) );

                    timeAreaSeries.Points.Clear();
                    timeAreaSeries.Points2.Clear();
                    timeAreaSeriesSmall.Points.Clear();
                    timeAreaSeriesSmall.Points2.Clear();

                    var startOfDay = TimeSpanAxis.ToDouble( new TimeSpan( 0, 0, dayData.StartOfDay ) );
                    var endOfDay = TimeSpanAxis.ToDouble( new TimeSpan( 0, 0, dayData.EndOfDay ) );
                    timeAreaSeries.Points.Add( new DataPoint( startOfDay, 0 ) );
                    timeAreaSeries.Points.Add( new DataPoint( startOfDay, 1 ) );
                    timeAreaSeries.Points2.Add( new DataPoint( endOfDay, 0 ) );
                    timeAreaSeries.Points2.Add( new DataPoint( endOfDay, 1 ) );
                    timeAreaSeriesSmall.Points.Add( new DataPoint( startOfDay, 0 ) );
                    timeAreaSeriesSmall.Points.Add( new DataPoint( startOfDay, 1 ) );
                    timeAreaSeriesSmall.Points2.Add( new DataPoint( endOfDay, 0 ) );
                    timeAreaSeriesSmall.Points2.Add( new DataPoint( endOfDay, 1 ) );

                    /*var currentTime = DateTimeAxis.ToDouble( DateTime.Now );
                    tempSource.Add( new DataPoint( currentTime, stateData.GetTemp ) );
                    humiSource.Add( new DataPoint( currentTime, stateData.GetHumi ) );

                    UpdateAreaSeries( tempAreaSeries, tempSource, tempData.High, tempData.Low );
                    UpdateAreaSeries( tempAreaMiniSeries, tempSource, tempData.High, tempData.Low );
                    UpdateAreaSeries( humiAreaSeries, humiSource, humiData.High, humiData.Low );
                    UpdateAreaSeries( humiAreaMiniSeries, humiSource, humiData.High, humiData.Low );*/

                    UpdateSource( tempSource, tempData.Values );
                    UpdateSource2( temp2Source, tempData.Values );
                    UpdateSource( humiSource, humiData.Values );
                    UpdateAreaSeries( tempAreaSeries, tempData.High0, tempData.Low0, tempData.Values );
                    UpdateAreaSeries( tempAreaMiniSeries, tempData.High0, tempData.Low0, tempData.Values );
                    UpdateAreaSeries( temp2AreaSeries, tempData.High1, tempData.Low1, tempData.Values );
                    UpdateAreaSeries( temp2AreaMiniSeries, tempData.High1, tempData.Low1, tempData.Values );
                    UpdateAreaSeries( humiAreaSeries, humiData.High, humiData.Low, humiData.Values );
                    UpdateAreaSeries( humiAreaMiniSeries, humiData.High, humiData.Low, humiData.Values );

                    if ( tempData.Values == null || tempData.Values.Length == 0 )
                        return;
                    if ( humiData.Values == null || humiData.Values.Length == 0 )
                        return;

                    this.Invoke( (Action<TerrariumInfo>)UpdateUIControls, info );
                }

                updateEvent.Set();
            }
        }

        String GetAddress()
        {
            return "http://" + addressControl.Text;
        }

        // -----------------------------------------------------------
        // Event handlers

        private void tempEditButton_Click( object sender, EventArgs e )
        {
            if ( tempEditing )
            {
                tempEditing = false;
                tempSaveButton.Visible = false;
                tempEditButton.Text = "Edit";
                tempMinControl.Enabled = false;
                tempMinControl.Value = tempMinStore;
                tempMaxControl.Enabled = false;
                tempMaxControl.Value = tempMaxStore;
            }
            else
            {
                tempEditing = true;
                tempSaveButton.Visible = true;
                tempEditButton.Text = "Cancel";
                tempMinControl.Enabled = true;
                tempMinStore = (int)tempMinControl.Value;
                tempMaxControl.Enabled = true;
                tempMaxStore = (int)tempMaxControl.Value;
            }
        }

        private void temp2EditButton_Click( object sender, EventArgs e )
        {
            if ( temp2Editing )
            {
                temp2Editing = false;
                temp2SaveButton.Visible = false;
                temp2EditButton.Text = "Edit";
                temp2MinControl.Enabled = false;
                temp2MinControl.Value = temp2MinStore;
                temp2MaxControl.Enabled = false;
                temp2MaxControl.Value = temp2MaxStore;
            }
            else
            {
                temp2Editing = true;
                temp2SaveButton.Visible = true;
                temp2EditButton.Text = "Cancel";
                temp2MinControl.Enabled = true;
                temp2MinStore = (int)temp2MinControl.Value;
                temp2MaxControl.Enabled = true;
                temp2MaxStore = (int)temp2MaxControl.Value;
            }
        }

        private void humiEditButton_Click( object sender, EventArgs e )
        {
            if ( humiEditing )
            {
                humiEditing = false;
                humiSaveButton.Visible = false;
                humiEditButton.Text = "Edit";
                humiMinControl.Enabled = false;
                humiMinControl.Value = humiMinStore;
                humiMaxControl.Enabled = false;
                humiMaxControl.Value = humiMaxStore;
            }
            else
            {
                humiEditing = true;
                humiSaveButton.Visible = true;
                humiEditButton.Text = "Cancel";
                humiMinControl.Enabled = true;
                humiMinStore = (int)humiMinControl.Value;
                humiMaxControl.Enabled = true;
                humiMaxStore = (int)humiMaxControl.Value;
            }
        }

        private void timeNowButton_Click( object sender, EventArgs e )
        {
            if ( timeEditing )
            {
                timeDateControl.Value = DateTime.Now;
                timeTimeControl.Text = String.Format( "{0:hh\\:mm}", DateTime.Now.TimeOfDay );
            }
        }

        private void timeEditButton_Click( object sender, EventArgs e )
        {
            if ( timeEditing )
            {
                timeEditing = false;
                timeEditButton.Text = "Edit";
                timeSaveButton.Visible = false;
                timeNowButton.Visible = false;
                timeDateControl.Enabled = false;
                timeTimeControl.Enabled = false;
                timeDayStartControl.Enabled = false;
                timeDayEndControl.Enabled = false;

                timeDateControl.Value = timeDateStore;
                timeTimeControl.Text = timeTimeStore;
                timeDayStartControl.Text = timeDayStartStore;
                timeDayEndControl.Text = timeDayEndStore;
            }
            else
            {
                timeEditing = true;
                timeEditButton.Text = "Cancel";
                timeSaveButton.Visible = true;
                timeNowButton.Visible = true;
                timeDateControl.Enabled = true;
                timeTimeControl.Enabled = true;
                timeDayStartControl.Enabled = true;
                timeDayEndControl.Enabled = true;

                timeDateStore = timeDateControl.Value;
                timeTimeStore = timeTimeControl.Text;
                timeDayStartStore = timeDayStartControl.Text;
                timeDayEndStore = timeDayEndControl.Text;
            }
        }

        private void pumpEditButton_Click( object sender, EventArgs e )
        {
            if ( pumpEditing )
            {
                pumpEditing = false;
                pumpSaveButton.Visible = false;
                pumpEditButton.Text = "Edit";
                pumpActiveForControl.Enabled = false;
                pumpActiveForControl.Value = pumpActiveForStore;
                pumpActivatedWaitControl.Enabled = false;
                pumpActivatedWaitControl.Value = pumpActivatedWaitStore;
            }
            else
            {
                pumpEditing = true;
                pumpSaveButton.Visible = true;
                pumpEditButton.Text = "Cancel";
                pumpActiveForControl.Enabled = true;
                pumpActiveForStore = (int)pumpActiveForControl.Value;
                pumpActivatedWaitControl.Enabled = true;
                pumpActivatedWaitStore = (int)pumpActivatedWaitControl.Value;
            }
        }

        private void updateEditButton_Click( object sender, EventArgs e )
        {
            if ( updateEditing )
            {
                updateEditing = false;
                updateSaveButton.Visible = false;
                updateEditButton.Text = "Edit";
                updateHistoryControl.Enabled = false;
                updateHistoryControl.Value = updateHistoryStore;
                updateSensorsControl.Enabled = false;
                updateSensorsControl.Value = updateSensorsStore;
            }
            else
            {
                updateEditing = true;
                updateSaveButton.Visible = true;
                updateEditButton.Text = "Cancel";
                updateHistoryControl.Enabled = true;
                updateHistoryStore = (int)updateHistoryControl.Value;
                updateSensorsControl.Enabled = true;
                updateSensorsStore = (int)updateSensorsControl.Value;
            }
        }

        void ForceUpdateUI()
        {
            Interlocked.Exchange( ref lastUpdate, Environment.TickCount - updateInterval );
            updateEvent.WaitOne();
        }

        async void tempSaveButton_Click( object sender, EventArgs e )
        {
            if ( tempEditing )
            {
                using ( var work = new TerrariumRequestWorker<TerrariumResponse>( "Setting temperature parameters..." ) )
                {
                    var address = GetAddress();
                    var store1 = tempMinStore;
                    var store2 = tempMaxStore;
                    var store3 = temp2MinStore;
                    var store4 = temp2MaxStore;
                    tempMinStore = (Int32)tempMinControl.Value;
                    tempMaxStore = (Int32)tempMaxControl.Value;
                    temp2MinStore = (Int32)temp2MinControl.Value;
                    temp2MaxStore = (Int32)temp2MaxControl.Value;

                    var res = await TerrariumAPI.SetTempAsync( address, tempMinStore, temp2MinStore, tempMaxStore, temp2MaxStore );

                    tempEditButton_Click( sender, e );

                    CheckRequestResult( res );

                    if ( !res.OK )
                    {
                        tempMinStore = store1;
                        tempMaxStore = store2;
                        temp2MinStore = store3;
                        temp2MaxStore = store4;
                    }

                    ForceUpdateUI();
                }
            }
        }

        async void temp2SaveButton_Click( object sender, EventArgs e )
        {
            if ( temp2Editing )
            {
                using ( var work = new TerrariumRequestWorker<TerrariumResponse>( "Setting temperature parameters..." ) )
                {
                    var address = GetAddress();
                    var store1 = tempMinStore;
                    var store2 = tempMaxStore;
                    var store3 = temp2MinStore;
                    var store4 = temp2MaxStore;
                    tempMinStore = (Int32)tempMinControl.Value;
                    tempMaxStore = (Int32)tempMaxControl.Value;
                    temp2MinStore = (Int32)temp2MinControl.Value;
                    temp2MaxStore = (Int32)temp2MaxControl.Value;

                    var res = await TerrariumAPI.SetTempAsync( address, tempMinStore, temp2MinStore, tempMaxStore, temp2MaxStore );

                    temp2EditButton_Click( sender, e );

                    CheckRequestResult( res );

                    if ( !res.OK )
                    {
                        tempMinStore = store1;
                        tempMaxStore = store2;
                        temp2MinStore = store3;
                        temp2MaxStore = store4;
                    }

                    ForceUpdateUI();
                }
            }
        }

        async void humiSaveButton_Click( object sender, EventArgs e )
        {
            if ( humiEditing )
            {
                using ( var work = new TerrariumRequestWorker<TerrariumResponse>( "Setting humidity parameters..." ) )
                {
                    var address = GetAddress();
                    var store1 = humiMinStore;
                    var store2 = humiMaxStore;
                    humiMinStore = (Int32)humiMinControl.Value;
                    humiMaxStore = (Int32)humiMaxControl.Value;

                    var res = await TerrariumAPI.SetHumiAsync( address, humiMinStore, humiMaxStore );

                    humiEditButton_Click( sender, e );

                    if ( !res.OK )
                    {
                        humiMinStore = store1;
                        humiMaxStore = store2;
                    }

                    ForceUpdateUI();
                }
            }
        }

        async void pumpSaveButton_Click( object sender, EventArgs e )
        {
            if ( pumpEditing )
            {
                using ( var work = new TerrariumRequestWorker<TerrariumResponse>( "Setting pump parameters..." ) )
                {
                    var address = GetAddress();
                    var store1 = pumpActiveForStore;
                    var store2 = pumpActivatedWaitStore;
                    pumpActiveForStore = (Int32)pumpActiveForControl.Value;
                    pumpActivatedWaitStore = (Int32)pumpActivatedWaitControl.Value;

                    var res = await TerrariumAPI.SetPumpAsync( address, pumpActiveForStore, pumpActivatedWaitStore );

                    pumpEditButton_Click( sender, e );

                    if ( !res.OK )
                    {
                        pumpActiveForStore = store1;
                        pumpActivatedWaitStore = store2;
                    }

                    ForceUpdateUI();
                }
            }
        }

        async void updateSaveButton_Click( object sender, EventArgs e )
        {
            if ( updateEditing )
            {
                using ( var work = new TerrariumRequestWorker<TerrariumResponse>( "Setting update parameters..." ) )
                {
                    var address = GetAddress();
                    var store1 = updateHistoryStore;
                    var store2 = updateSensorsStore;
                    updateHistoryStore = (Int32)updateHistoryControl.Value;
                    updateSensorsStore = (Int32)updateSensorsControl.Value;

                    var res = await TerrariumAPI.SetUpdatesAsync( address, updateHistoryStore, updateSensorsStore );

                    updateEditButton_Click( sender, e );

                    if ( !res.OK )
                    {
                        updateHistoryStore = store1;
                        updateSensorsStore = store2;
                    }

                    ForceUpdateUI();
                }
            }
        }

        async void timeSaveButton_Click( object sender, EventArgs e )
        {
            if ( timeEditing )
            {
                using ( var work = new TerrariumRequestWorker<TerrariumResponse>( "Setting time parameters..." ) )
                {
                    var date = timeDateControl.Value.Date;
                    var time = TimeSpan.Parse( timeTimeControl.Text );
                    date = date.Add( time );

                    var address = GetAddress();
                    var store1 = timeDateStore;
                    var store2 = timeTimeStore;
                    var store3 = timeDayStartStore;
                    var store4 = timeDayEndStore;

                    timeDateStore = date;
                    timeTimeStore = time.ToString( "hh\\:mm" );
                    timeDayStartStore = timeDayStartControl.Text;
                    timeDayEndStore = timeDayEndControl.Text;

                    timeEditButton_Click( sender, e );

                    var res = await TerrariumAPI.SetTimeAsync( address, TerrariumAPI.DateTimeToUNIXTime( date ) );

                    if ( !res.OK )
                    {
                        timeDateStore = store1;
                        timeTimeStore = store2;
                    }

                    res = await TerrariumAPI.SetDayAsync( address, (Int32)TimeSpan.Parse( timeDayStartStore ).TotalSeconds, (Int32)TimeSpan.Parse( timeDayEndStore ).TotalSeconds );

                    if ( !res.OK )
                    {
                        timeDayStartStore = store3;
                        timeDayEndStore = store4;
                    }

                    ForceUpdateUI();
                }
            }
        }

        private void temperaturePanel2_Click( object sender, EventArgs e )
        {
            panel6.BackColor = this.BackColor;
            panel6.BorderStyle = BorderStyle.None;
            panel7.BackColor = this.BackColor;
            panel7.BorderStyle = BorderStyle.None;
            panel9.BackColor = this.BackColor;
            panel9.BorderStyle = BorderStyle.None;
            temperaturePanel.BackColor = this.BackColor;
            temperaturePanel.BorderStyle = BorderStyle.None;

            temperaturePanel2.BackColor = System.Drawing.Color.AliceBlue;
            temperaturePanel2.BorderStyle = BorderStyle.FixedSingle;

            temp2Group.Visible = true;
            tempGroup.Visible = false;
            humiGroup.Visible = false;
            timeGroup.Visible = false;
            otherGroup.Visible = false;
        }

        private void panel9_Click( object sender, EventArgs e )
        {
            panel6.BackColor = this.BackColor;
            panel6.BorderStyle = BorderStyle.None;
            panel7.BackColor = this.BackColor;
            panel7.BorderStyle = BorderStyle.None;
            temperaturePanel.BackColor = this.BackColor;
            temperaturePanel.BorderStyle = BorderStyle.None;
            temperaturePanel2.BackColor = this.BackColor;
            temperaturePanel2.BorderStyle = BorderStyle.None;

            panel9.BackColor = System.Drawing.Color.AliceBlue;
            panel9.BorderStyle = BorderStyle.FixedSingle;

            tempGroup.Visible = false;
            temp2Group.Visible = false;
            humiGroup.Visible = false;
            timeGroup.Visible = false;
            otherGroup.Visible = true;
        }

        private void panel7_Click( object sender, EventArgs e )
        {
            panel6.BackColor = this.BackColor;
            panel6.BorderStyle = BorderStyle.None;
            panel9.BackColor = this.BackColor;
            panel9.BorderStyle = BorderStyle.None;
            temperaturePanel.BackColor = this.BackColor;
            temperaturePanel.BorderStyle = BorderStyle.None;
            temperaturePanel2.BackColor = this.BackColor;
            temperaturePanel2.BorderStyle = BorderStyle.None;

            panel7.BackColor = System.Drawing.Color.AliceBlue;
            panel7.BorderStyle = BorderStyle.FixedSingle;

            tempGroup.Visible = false;
            temp2Group.Visible = false;
            humiGroup.Visible = false;
            timeGroup.Visible = true;
            otherGroup.Visible = false;
        }

        private void panel5_Click( object sender, EventArgs e )
        {
            panel6.BackColor = this.BackColor;
            panel6.BorderStyle = BorderStyle.None;
            panel7.BackColor = this.BackColor;
            panel7.BorderStyle = BorderStyle.None;
            panel9.BackColor = this.BackColor;
            panel9.BorderStyle = BorderStyle.None;
            temperaturePanel2.BackColor = this.BackColor;
            temperaturePanel2.BorderStyle = BorderStyle.None;

            temperaturePanel.BackColor = System.Drawing.Color.AliceBlue;
            temperaturePanel.BorderStyle = BorderStyle.FixedSingle;

            tempGroup.Visible = true;
            temp2Group.Visible = false;
            humiGroup.Visible = false;
            timeGroup.Visible = false;
            otherGroup.Visible = false;
        }

        private void panel6_Click( object sender, EventArgs e )
        {
            temperaturePanel.BackColor = this.BackColor;
            temperaturePanel.BorderStyle = BorderStyle.None;
            panel7.BackColor = this.BackColor;
            panel7.BorderStyle = BorderStyle.None;
            panel9.BackColor = this.BackColor;
            panel9.BorderStyle = BorderStyle.None;
            temperaturePanel2.BackColor = this.BackColor;
            temperaturePanel2.BorderStyle = BorderStyle.None;

            panel6.BackColor = System.Drawing.Color.AliceBlue;
            panel6.BorderStyle = BorderStyle.FixedSingle;

            tempGroup.Visible = false;
            temp2Group.Visible = false;
            humiGroup.Visible = true;
            timeGroup.Visible = false;
            otherGroup.Visible = false;
        }

        void UpdateSource( List<DataPoint> source, Int32[][] data )
        {
            source.Clear();

            if ( data == null || data.Length == 0 )
                return;

            for ( var i = 0; i < data.Length; i++ )
                source.Add( new DataPoint( DateTimeAxis.ToDouble( TerrariumAPI.UNIXTimeToDateTime( data[ i ][ 0 ] ) ), data[ i ][ 1 ] ) );
        }

        void UpdateSource2( List<DataPoint> source, Int32[][] data )
        {
            source.Clear();

            if ( data == null || data.Length == 0 )
                return;

            for ( var i = 0; i < data.Length; i++ )
                source.Add( new DataPoint( DateTimeAxis.ToDouble( TerrariumAPI.UNIXTimeToDateTime( data[ i ][ 0 ] ) ), data[ i ][ 2 ] ) );
        }

        void UpdateAreaSeries( AreaSeries area, List<DataPoint> source, Int32 high, Int32 low )
        {
            if ( source == null || source.Count == 0 )
                return;

            var firstX = source[ 0 ].X;
            var lastX = source[ source.Count - 1 ].X;

            area.Points.Clear();
            area.Points2.Clear();
            area.Points.Add( new DataPoint( firstX, high ) );
            area.Points.Add( new DataPoint( lastX, high ) );
            area.Points2.Add( new DataPoint( firstX, low ) );
            area.Points2.Add( new DataPoint( lastX, low ) );
        }

        void UpdateAreaSeries( AreaSeries area, Int32 high, Int32 low, Int32[][] data )
        {
            if ( data == null || data.Length == 0 )
                return;

            var firstX = DateTimeAxis.ToDouble( TerrariumAPI.UNIXTimeToDateTime( data[ 0 ][ 0 ] ) );
            var lastX = DateTimeAxis.ToDouble( TerrariumAPI.UNIXTimeToDateTime( data[ data.Length - 1 ][ 0 ] ) );

            area.Points.Clear();
            area.Points2.Clear();
            area.Points.Add( new DataPoint( firstX, high ) );
            area.Points.Add( new DataPoint( lastX, high ) );
            area.Points2.Add( new DataPoint( firstX, low ) );
            area.Points2.Add( new DataPoint( lastX, low ) );
        }
    }

    struct TerrariumRequestWorker<TResult> : IDisposable
    {
        WorkDialog diag;

        public TerrariumRequestWorker( string text )
        {
            diag = new WorkDialog();
            diag.SetText( text );
            diag.Show();
        }

        public Task<TResult> Run( Func<TResult> action )
        {
            return Task<TResult>.Run<TResult>( action );
        }

        void IDisposable.Dispose()
        {
            diag.Dispose();
        }
    }


}
